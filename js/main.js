var viewer = new Marzipano.Viewer(document.getElementById('pano'));
var deviceOrientationControlMethod = new DeviceOrientationControlMethod();
var controls = viewer.controls();
controls.registerMethod('deviceOrientation', deviceOrientationControlMethod);
var geometry = new Marzipano.EquirectGeometry([{
    width: 4096
}]);
var limiter = Marzipano.RectilinearView.limit.traditional(2048, 100 * Math.PI / 180);
var view = new Marzipano.RectilinearView({
    yaw: Math.PI
}, limiter);
var source = Marzipano.ImageUrlSource.fromString(
    "img/img.jpg"
);
// Create scene.
var scene = viewer.createScene({
    source: source,
    geometry: geometry,
    view: view,
    pinFirstLevel: true
});
// initiates view
scene.switchTo();



// add hotspots

//St Andrew's House reveal info
scene.hotspotContainer().createHotspot(document.querySelector("#reveal1"), {
    yaw: -1.5,
    pitch: -0.5
});

//Ideas Factory
scene.hotspotContainer().createHotspot(document.querySelector("#reveal2"), {
    yaw: -0.8,
    pitch: -0.25
});

//SU lounge
scene.hotspotContainer().createHotspot(document.querySelector("#textInfo1"), {
    yaw: -2.2,
    pitch: -0.08
});

//St Andrews
scene.hotspotContainer().createHotspot(document.querySelector("#textInfo2"), {
    yaw: 2,
    pitch: -0.3
});

//Ameer
scene.hotspotContainer().createHotspot(document.querySelector("#textInfo3"), {
    yaw: 0.25,
    pitch: -0.25
});

//Jason
scene.hotspotContainer().createHotspot(document.querySelector("#textInfo4"), {
    yaw: 0.42,
    pitch: -0.275
});

//Namii
scene.hotspotContainer().createHotspot(document.querySelector("#textInfo5"), {
    yaw: 0.55,
    pitch: -0.35
});



// Set up control for enabling/disabling device orientation.
var enabled = false;
var toggleElement = document.getElementById('toggleDeviceOrientation');

function enable() {
    deviceOrientationControlMethod.getPitch(function (err, pitch) {
        if (!err) {
            view.setPitch(pitch);
        }
    });
    controls.enableMethod('deviceOrientation');
    enabled = true;
    toggleElement.className = 'enabled';
}

function disable() {
    controls.disableMethod('deviceOrientation');
    enabled = false;
    toggleElement.className = '';
}

function toggle() {
    if (enabled) {
        disable();
    } else {
        enable();
    }
}
toggleElement.addEventListener('click', toggle);

